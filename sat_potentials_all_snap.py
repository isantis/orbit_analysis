#!/usr/bin/env python3
##SBATCH --job-name=subhalo_potential_all_snaps
#SBATCH --job-name=LG_subhalo_potential_all_snaps
##SBATCH --partition=high2m    # peloton high-mem node: 32 cores, 15.6 GB per core, 500 GB total
##SBATCH --partition=high2
#SBATCH --partition=skx-normal
##SBATCH --mem=500G
##SBATCH --mem=100G
#SBATCH --nodes=1
##SBATCH --ntasks=3    # processes total
#SBATCH --tasks-per-node=1    # MPI tasks per node
##SBATCH --cpus-per-task=1    # OpenMP threads per MPI task
#SBATCH --time=03:00:00
##SBATCH --output=/home/ibsantis/scripts/jobs/potentials/all_snapshots/subhalo_potential_all_snaps_%j.txt
#SBATCH --output=/home1/05400/ibsantis/scripts/jobs/potentials/all_snapshots/LG_subhalo_potential_all_snaps_%j.txt
#SBATCH --mail-user=ibsantistevan@ucdavis.edu
#SBATCH --mail-type=fail
#SBATCH --mail-type=end
#SBATCH --mail-type=begin
#SBATCH --account=TG-AST140064

"""

    ======================
    = Subhalo potentials =
    ======================

    Calculate:
        - The host halo potential at 2*R_200m +/- 5 kpc using ALL particles
        - Each subhalo potential at +/- 5 kpc from their radius using DM particles

    NOTES:
        - The mean and median host potential are almost identical
        - The potential of the host galaxy using only DM particles is
          almost identical.
        - The subhalo potential using R-5kpc < d < R gives almost the same
          results as when doing R +/- 5 kpc

    COULDNT RUN ON M12Z OR ROMULUS & REMUS!

"""

# Import packages
import orbit_io
import summary_io
import halo_analysis as halo
import gizmo_analysis as gizmo
import utilities as ut
import numpy as np
import time
import sys
print('Read in the tools')

### Set path and initial parameters
loc = 'stampede'
host = sys.argv[1]
sim_data = orbit_io.OrbitRead(gal1=str(host), location=loc)
print('Set paths')

# Read in snapshot dictionary and the halo tree
snaps = ut.simulation.read_snapshot_times(directory=sim_data.simulation_dir) # Saves snapshots, redshifts, lookback times, etc. to an array
halt = halo.io.IO.read_tree(simulation_directory=sim_data.simulation_dir, file_kind='hdf5', species='star', host_number=sim_data.num_gal)
print('Read in halo tree and set up subhalo indices')

# Set up some of the parameters to the function
summary = summary_io.SummaryDataSort()
data_total = summary.data_read(directory=sim_data.home_dir, hosts='lg', sim_type='baryon')
#
if sim_data.num_gal == 1:
    orbits = orbit_io.OrbitAnalysis(tree=halt, gal1=sim_data.galaxy, location=loc, host=1)
    host_radius = data_total[sim_data.galaxy]['host.radius'][0]
    host_mass = data_total[sim_data.galaxy]['host.mass'][0]
#
if sim_data.num_gal == 2:
    orbits_1 = orbit_io.OrbitAnalysis(tree=halt, gal1=sim_data.gal_1, location=loc, host=1)
    orbits_2 = orbit_io.OrbitAnalysis(tree=halt, gal1=sim_data.gal_1, location=loc, host=2)
    host_radius = data_total[sim_data.gal_1]['host.radius'][0]
    host_mass = data_total[sim_data.gal_1]['host.mass'][0]
    host_radius2 = data_total[sim_data.gal_2]['host.radius'][0]
    host_mass2 = data_total[sim_data.gal_2]['host.mass'][0]

# Set up the snapshot array to loop through
snaps = np.arange(int(sys.argv[2]), int(sys.argv[3]), -1)
print('Assigned snapshot array')

# Define the function which calculates the potential for all snapshots
# Also calculates other stuff like: U(R200m), KE_c, U(100kpc,z)
def calc_sub_potential(snap, simdata, orbit_class, R200m, M200m, orbit_class2=None, R200m2=None, M200m2=None):
    #
    if simdata.num_gal == 1:
        #
        # Set up the host indices
        host_inds = halt.prop('progenitor.main.indices', halt['host.index'][0])
        host_snaps = halt['snapshot'][host_inds]
        print('Defined the times the host exists')
        #
        if snap in host_snaps:
            # Read in the snapshot dictionary, halo tree, and z = 0 snapshot
            start = time.time()
            #
            # For luminous & ALL subhalos
            part = gizmo.io.Read.read_snapshots('dark', 'index', snap, properties=['position', 'potential'], simulation_directory=simdata.simulation_dir, assign_hosts_rotation=True)
            end = time.time()
            print('Particles at snapshot {0} read in in {1} seconds'.format(snap, end-start))
            #
            # # Set up the halo inds and KDTree
            start = time.time()
            orbit_tree = orbit_io.OrbitTree(tree=halt, gal1=simdata.galaxy, location=loc, host=1, particles=part, subsampling=15)
            end = time.time()
            print('KDTree created in {0} seconds'.format(end-start))
            #
            # Create a binning scheme and mass bin array to loop over
            def mass_binning(mass_array, mass_range):
                mass_array = np.log10(mass_array)
                mask = (mass_array > mass_range[0])*(mass_array < mass_range[1])
                return mask
            #
            halo_mass_bins = np.array([7., 7.5, 8., 8.5, 9., 9.5, 10., 10.5, 11., 11.5, 12.])
            #
            # Create a dictionary to save the data to
            data_dict = dict()
            data_dict['mass.bin'] = halo_mass_bins
            data_dict['halo.inds'] = (-1)*np.ones(len(orbit_class.sub_inds[:,600-snap]), dtype=int)
            data_dict['halo.bin'] = (-1)*np.ones(len(orbit_class.sub_inds[:,600-snap]), dtype=int)
            data_dict['subhalo.potential'] = (-1)*np.ones(len(orbit_class.sub_inds[:,600-snap]))
            data_dict['particle.num'] = (-1)*np.ones(len(orbit_class.sub_inds[:,600-snap]), dtype=int)
            temp = np.arange(len(orbit_class.sub_inds[:,600-snap]))
            print('Set up a null dictionary for the data at snapshot {0}'.format(snap))
            #
            # Calculate the host potential at 500 kpc at each snapshot
            ndist, nind = orbit_tree.neighbors(centers=halt['position'][host_inds[600-snap]]/(1+part.snapshot['redshift']), neigh_num_max=1e8, neigh_dist_max=500+5, workerss=4)
            part_mask = (ndist[np.isfinite(ndist)] < (500+5))*(ndist[np.isfinite(ndist)] > (500-5))
            data_dict['host.potential.500kpc'] = np.nanmean(part['dark']['potential'][::orbit_tree.subsampling][nind[np.isfinite(ndist)][part_mask]])
            #
            if snap == 600:
                # Find the potential of the host within R200
                ndist, nind = orbit_tree.neighbors(centers=halt['position'][halt['host.index'][0]]/(1+part.snapshot['redshift']), neigh_num_max=1e8, neigh_dist_max=R200m+5, workerss=4)
                part_mask = (ndist[np.isfinite(ndist)] < (R200m+5))*(ndist[np.isfinite(ndist)] > (R200m-5))
                data_dict['host.potential.R200m'] = np.nanmean(part['dark']['potential'][::orbit_tree.subsampling][nind[np.isfinite(ndist)][part_mask]])
                data_dict['host.particle.num'] = np.sum(part_mask)
                G = ((6.67*10**(-11)*2*10**(30))/(1000*3.086*10**(16)*1000**2))
                data_dict['KE.at.Rvir'] = 0.5*G*(M200m/R200m)
                #
                print('Finished calculating host data for snapshot 600')
            #
            # Loop over each mass bin
            print('Starting loop over the mass bins')
            for i in range(0, len(halo_mass_bins)-1):
                start = time.time()
                # Get the halos in a mass bin
                real_halos = (orbit_class.sub_inds[:,600-snap] >= 0)
                mass_mask = mass_binning(halt.prop('mass.peak', orbit_class.sub_inds[:,600-snap]), (halo_mass_bins[i], halo_mass_bins[i+1]))
                data_dict['halo.inds'][real_halos*mass_mask] = orbit_class.sub_inds[:,600-snap][real_halos*mass_mask]
                data_dict['halo.bin'][real_halos*mass_mask] = i
                #
                # If there are halos, continue
                if np.sum(mass_mask) != 0:
                    # Get the halo positions and max halo radius in the mass bin
                    halo_pos = halt['position'][orbit_class.sub_inds[:,600-snap][real_halos*mass_mask]]/(1+part.snapshot['redshift'])
                    dmax = np.around(np.max(halt['radius'][orbit_class.sub_inds[:,600-snap][real_halos*mass_mask]]))+5
                    #
                    # Query the particle tree and save the distances and indices
                    ndist, nind = orbit_tree.neighbors(centers=halo_pos, neigh_num_max=1e7, neigh_dist_max=dmax, workerss=4)
                    #
                    # Loop over the number of halos
                    for j in range(0, len(halo_pos)):
                        # Find the particles within +/- 5 kpc of the halo radius, then save the potential and particle number
                        part_mask = (ndist[j][np.isfinite(ndist[j])] < (halt['radius'][orbit_class.sub_inds[:,600-snap][real_halos*mass_mask]][j]+5))*(ndist[j][np.isfinite(ndist[j])] > (halt['radius'][orbit_class.sub_inds[:,600-snap][real_halos*mass_mask]][j]-5))
                        data_dict['subhalo.potential'][temp[real_halos*mass_mask][j]] = np.nanmean(part['dark']['potential'][::orbit_tree.subsampling][nind[j][np.isfinite(ndist[j])][part_mask]])
                        data_dict['particle.num'][temp[real_halos*mass_mask][j]] = np.sum(part_mask)
                #
                # If no halos, say so
                else:
                    print('No halos between {0} and {1}'.format(halo_mass_bins[i],halo_mass_bins[i+1]))
                end = time.time()
                #
                print('Done with mass bin {0} in {1} seconds'.format(i, end-start))
            #
            ut.io.file_hdf5(file_name_base=simdata.home_dir+'/orbit_data/hdf5_files/potentials/all_snapshots/'+simdata.galaxy+'/'+simdata.galaxy+'_potentials_'+str(snap), dict_or_array_to_write=data_dict, verbose=True)
            print('Completely finished writing snapshot {0}'.format(snap))
        #
        if snap not in host_snaps:
            print('No well defined host at snapshot {0}'.format(snap))

    if simdata.num_gal == 2:
        #
        # Set up the host indices
        host_inds_1 = halt.prop('progenitor.main.indices', halt['host.index'][0])
        host_snaps_1 = halt['snapshot'][host_inds_1]
        host_inds_2 = halt.prop('progenitor.main.indices', halt['host2.index'][0])
        host_snaps_2 = halt['snapshot'][host_inds_2]
        #
        start = time.time()
        #
        # For luminous & ALL subhalos
        part = gizmo.io.Read.read_snapshots('dark', 'index', snap, properties=['position', 'potential'], simulation_directory=simdata.simulation_dir, assign_hosts_rotation=True)
        end = time.time()
        print('Particles at snapshot {0} read in in {1} seconds'.format(snap, end-start))
        #
        # Set up the halo inds and KDTree
        start = time.time()
        orbit_tree_1 = orbit_io.OrbitTree(tree=halt, gal1=simdata.gal_1, location=loc, host=1, particles=part, subsampling=15)
        orbit_tree_2 = orbit_io.OrbitTree(tree=halt, gal1=simdata.gal_1, location=loc, host=2, particles=part, subsampling=15)
        end = time.time()
        print('KDTree created in {0} seconds'.format(end-start))
        #
        # Create a binning scheme and mass bin array to loop over
        def mass_binning(mass_array, mass_range):
            mass_array = np.log10(mass_array)
            mask = (mass_array > mass_range[0])*(mass_array < mass_range[1])
            return mask
        #
        halo_mass_bins = np.array([7., 7.5, 8., 8.5, 9., 9.5, 10., 10.5, 11., 11.5, 12.])
        #
        # Create a dictionary to save the data to
        data_dict_1 = dict()
        data_dict_1['mass.bin'] = halo_mass_bins
        data_dict_1['halo.inds'] = (-1)*np.ones(len(orbit_class.sub_inds[:,600-snap]), dtype=int)
        data_dict_1['halo.bin'] = (-1)*np.ones(len(orbit_class.sub_inds[:,600-snap]), dtype=int)
        data_dict_1['subhalo.potential'] = (-1)*np.ones(len(orbit_class.sub_inds[:,600-snap]))
        data_dict_1['particle.num'] = (-1)*np.ones(len(orbit_class.sub_inds[:,600-snap]), dtype=int)
        temp_1 = np.arange(len(orbit_class.sub_inds[:,600-snap]))
        #
        data_dict_2 = dict()
        data_dict_2['mass.bin'] = halo_mass_bins
        data_dict_2['halo.inds'] = (-1)*np.ones(len(orbit_class2.sub_inds[:,600-snap]), dtype=int)
        data_dict_2['halo.bin'] = (-1)*np.ones(len(orbit_class2.sub_inds[:,600-snap]), dtype=int)
        data_dict_2['subhalo.potential'] = (-1)*np.ones(len(orbit_class2.sub_inds[:,600-snap]))
        data_dict_2['particle.num'] = (-1)*np.ones(len(orbit_class2.sub_inds[:,600-snap]), dtype=int)
        temp_2 = np.arange(len(orbit_class2.sub_inds[:,600-snap]))
        #
        print('Set up a null dictionary for the data at snapshot {0}'.format(snap))
        #
        if snap not in host_snaps_1:
            print('No well defined host at snapshot {0}'.format(snap))
        #
        if snap not in host_snaps_2:
            print('No well defined host at snapshot {0}'.format(snap))
        #
        # Calculate the host potential at 500 kpc at each snapshot
        ndist, nind = orbit_tree_1.neighbors(centers=halt['position'][host_inds_1[600-snap]]/(1+part.snapshot['redshift']), neigh_num_max=1e8, neigh_dist_max=500+5, workerss=4)
        part_mask = (ndist[np.isfinite(ndist)] < (500+5))*(ndist[np.isfinite(ndist)] > (500-5))
        data_dict_1['host.potential.500kpc'] = np.nanmean(part['dark']['potential'][::orbit_tree_1.subsampling][nind[np.isfinite(ndist)][part_mask]])
        #
        ndist, nind = orbit_tree_2.neighbors(centers=halt['position'][host_inds_2[600-snap]]/(1+part.snapshot['redshift']), neigh_num_max=1e8, neigh_dist_max=500+5, workerss=4)
        part_mask = (ndist[np.isfinite(ndist)] < (500+5))*(ndist[np.isfinite(ndist)] > (500-5))
        data_dict_2['host.potential.500kpc'] = np.nanmean(part['dark']['potential'][::orbit_tree_2.subsampling][nind[np.isfinite(ndist)][part_mask]])
        #
        if snap == 600:
            # Find the potential of the host within R200
            ndist_1, nind_1 = orbit_tree_1.neighbors(centers=halt['position'][halt['host.index'][0]]/(1+part.snapshot['redshift']), neigh_num_max=1e8, neigh_dist_max=R200m+5, workerss=4)
            part_mask_1 = (ndist_1[np.isfinite(ndist_1)] < (R200m+5))*(ndist_1[np.isfinite(ndist_1)] > (R200m-5))
            data_dict_1['host.potential.R200m'] = np.nanmean(part['dark']['potential'][::orbit_tree_1.subsampling][nind_1[np.isfinite(ndist_1)][part_mask_1]])
            data_dict_1['host.particle.num'] = np.sum(part_mask_1)
            G = ((6.67*10**(-11)*2*10**(30))/(1000*3.086*10**(16)*1000**2))
            data_dict_1['KE.at.Rvir'] = 0.5*G*(M200m/R200m)
            #
            # Find the potential of the host within R200
            ndist_2, nind_2 = orbit_tree_2.neighbors(centers=halt['position'][halt['host2.index'][0]]/(1+part.snapshot['redshift']), neigh_num_max=1e8, neigh_dist_max=R200m2+5, workerss=4)
            part_mask_2 = (ndist_2[np.isfinite(ndist_2)] < (R200m2+5))*(ndist_2[np.isfinite(ndist_2)] > (R200m2-5))
            data_dict_2['host.potential.R200m'] = np.nanmean(part['dark']['potential'][::orbit_tree_2.subsampling][nind_2[np.isfinite(ndist_2)][part_mask_2]])
            data_dict_2['host.particle.num'] = np.sum(part_mask_2)
            G = ((6.67*10**(-11)*2*10**(30))/(1000*3.086*10**(16)*1000**2))
            data_dict_2['KE.at.Rvir'] = 0.5*G*(M200m2/R200m2)
            #
            print('Finished calculating host data for snapshot 600')
        #
        # Loop over each mass bin
        print('Starting loop over the mass bins')
        for i in range(0, len(halo_mass_bins)-1):
            start = time.time()
            # Get the halos in a mass bin
            real_halos_1 = (orbit_class.sub_inds[:,600-snap] >= 0)
            mass_mask_1 = mass_binning(halt.prop('mass.peak', orbit_class.sub_inds[:,600-snap]), (halo_mass_bins[i], halo_mass_bins[i+1]))
            #
            real_halos_2 = (orbit_class2.sub_inds[:,600-snap] >= 0)
            mass_mask_2 = mass_binning(halt.prop('mass.peak', orbit_class2.sub_inds[:,600-snap]), (halo_mass_bins[i], halo_mass_bins[i+1]))
            #
            data_dict_1['halo.inds'][real_halos_1*mass_mask_1] = orbit_class.sub_inds[:,600-snap][real_halos_1*mass_mask_1]
            data_dict_1['halo.bin'][real_halos_1*mass_mask_1] = i
            #
            data_dict_2['halo.inds'][real_halos_2*mass_mask_2] = orbit_class2.sub_inds[:,600-snap][real_halos_2*mass_mask_2]
            data_dict_2['halo.bin'][real_halos_2*mass_mask_2] = i
            #
            # If there are halos, continue
            if np.sum(mass_mask_1) != 0:
                # Get the halo positions and max halo radius in the mass bin
                halo_pos_1 = halt['position'][orbit_class.sub_inds[:,600-snap][real_halos_1*mass_mask_1]]/(1+part.snapshot['redshift'])
                dmax_1 = np.around(np.max(halt['radius'][orbit_class.sub_inds[:,600-snap][real_halos_1*mass_mask_1]]))+5
                #
                # Query the particle tree and save the distances and indices
                ndist_1, nind_1 = orbit_tree_1.neighbors(centers=halo_pos_1, neigh_num_max=1e7, neigh_dist_max=dmax_1, workerss=4)
                #
                # Loop over the number of halos
                for j in range(0, len(halo_pos_1)):
                    # Find the particles within +/- 5 kpc of the halo radius, then save the potential and particle number
                    part_mask_1 = (ndist_1[j][np.isfinite(ndist_1[j])] < (halt['radius'][orbit_class.sub_inds[:,600-snap][real_halos_1*mass_mask_1]][j]+5))*(ndist_1[j][np.isfinite(ndist_1[j])] > (halt['radius'][orbit_class.sub_inds[:,600-snap][real_halos_1*mass_mask_1]][j]-5))
                    data_dict_1['subhalo.potential'][temp_1[real_halos_1*mass_mask_1][j]] = np.nanmean(part['dark']['potential'][::orbit_tree_1.subsampling][nind_1[j][np.isfinite(ndist_1[j])][part_mask_1]])
                    data_dict_1['particle.num'][temp_1[real_halos_1*mass_mask_1][j]] = np.sum(part_mask_1)
            #
            if np.sum(mass_mask_2) != 0:
                # Get the halo positions and max halo radius in the mass bin
                halo_pos_2 = halt['position'][orbit_class2.sub_inds[:,600-snap][real_halos_2*mass_mask_2]]/(1+part.snapshot['redshift'])
                dmax_2 = np.around(np.max(halt['radius'][orbit_class2.sub_inds[:,600-snap][real_halos_2*mass_mask_2]]))+5
                #
                # Query the particle tree and save the distances and indices
                ndist_2, nind_2 = orbit_tree_2.neighbors(centers=halo_pos_2, neigh_num_max=1e7, neigh_dist_max=dmax_2, workerss=4)
                #
                # Loop over the number of halos
                for j in range(0, len(halo_pos_2)):
                    # Find the particles within +/- 5 kpc of the halo radius, then save the potential and particle number
                    part_mask_2 = (ndist_2[j][np.isfinite(ndist_2[j])] < (halt['radius'][orbit_class2.sub_inds[:,600-snap][real_halos_2*mass_mask_2]][j]+5))*(ndist_2[j][np.isfinite(ndist_2[j])] > (halt['radius'][orbit_class2.sub_inds[:,600-snap][real_halos_2*mass_mask_2]][j]-5))
                    data_dict_2['subhalo.potential'][temp_2[real_halos_2*mass_mask_2][j]] = np.nanmean(part['dark']['potential'][::orbit_tree_2.subsampling][nind_2[j][np.isfinite(ndist_2[j])][part_mask_2]])
                    data_dict_2['particle.num'][temp_2[real_halos_2*mass_mask_2][j]] = np.sum(part_mask_2)
            #
            # If no halos, say so
            else:
                print('No halos between {0} and {1}'.format(halo_mass_bins[i],halo_mass_bins[i+1]))
            end = time.time()
            print('Done with mass bin {0} in {1} seconds'.format(i, end-start))
        #
        ut.io.file_hdf5(file_name_base=simdata.home_dir+'/orbit_data/hdf5_files/potentials/all_snapshots/'+simdata.gal_1+'/'+simdata.gal_1+'_potentials_'+str(snap), dict_or_array_to_write=data_dict_1, verbose=True)
        ut.io.file_hdf5(file_name_base=simdata.home_dir+'/orbit_data/hdf5_files/potentials/all_snapshots/'+simdata.gal_2+'/'+simdata.gal_2+'_potentials_'+str(snap), dict_or_array_to_write=data_dict_2, verbose=True)
        print('Completely finished writing snapshot {0}'.format(snap))


print('Setting up arguments')
# Create an array of arguments for the function above
if sim_data.num_gal == 1:
    args_list = [(snapshot, sim_data, orbits, host_radius, host_mass) for snapshot in snaps]
if sim_data.num_gal == 2:
    args_list = [(snapshot, sim_data, orbits_1, host_radius, host_mass, orbits_2, host_radius2, host_mass2) for snapshot in snaps]
print('Starting to run on data')
# Run the function using the arguments above in parallel
ut.io.run_in_parallel(calc_sub_potential, args_list, proc_number=1, verbose=True) # ADD VERBOSE

print('All done.')
