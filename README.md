# Description

Python package for analyzing the FIRE simulations of MW-mass galaxies, and in
particular, includes tools for modeling mass/density profiles of the MW-mass hosts,
deriving orbital properties of satellites, and analyzing data generated by these
methods. 

---

# Requirements

This package makes use of the following:

- Python 3
- [gizmo_analysis](https://bitbucket.org/awetzel/gizmo_analysis)
- [halo_analysis](https://bitbucket.org/awetzel/halo_analysis)
- [utilities](https://bitbucket.org/awetzel/utilities)
- [numpy](https://numpy.org/)
- [scipy](https://scipy.org/)
- [astropy](https://www.astropy.org/)
- [matplotlib](https://matplotlib.org/)

Most importantly, this package uses the FIRE-2 simulations. A handful of snapshot data, and halo catalogs, for the MW-mass galaxies are hosted [here](https://flathub.flatironinstitute.org/fire) at the Flatiron Institute. For a brief description of the simulations and everything included in the publically available data, see this [public data release paper](https://ui.adsabs.harvard.edu/abs/2023ApJS..265...44W/abstract).

---

# Contents

## model_io.py
* This was written to help compute parameters for different density / mass models for:
    - Double exponential disk
        - This accounts for an "inner" (bulge-like) and "outer" disk region
    - NFW halo
    - Generalized NFW halo with an inner and outer slope

## orbit_io.py
* This package contains multiple classes designed to ultimately calculate orbit parameters of simulated satellite galaxies.

## summary_io.py
* This was written to create summary statistic plots with data output from orbit_io.py and model_io.py. 
    - Data that I import was previously compiled using summary_data.py or summary_data_dmo.py

## satellite_io.py
* For finding subhalo analogs to MW satellites, calculating their weights, saving analogs to a file. With analogs, create a dictionary of orbit history properties for statistical analysis.

## summary_data.py
* For making data dictionaries to be used with summary_io.py.

## paper_figs_final.py
* Script that contains code for figures in [this paper](https://arxiv.org/abs/2208.05977) focused on calculating the orbital dynamics and histories of satellite galaxies aroung MW-mass host galaxies.

## paper_ii_figs_final.py
* Script that contains code for figures in [this paper](https://arxiv.org/abs/2309.05708) focused on comparing orbit and infall history properties from cosmological simulations to static potential idealized models.

---

# Units

Unless otherwise explicitly stated, all units in these packages follow the same convention as the FIRE-2 simulations. Here are some of the most common property units:

- mass: M_{sun}
- distance: kpc
- time: Gyr [10^9 yr]
- velocity: km s^{-1}
- energy: km^2 s^{-2}

---

# Installing

You can install this package by using the following command:

```python
git clone https://isantis@bitbucket.org/isantis/orbit_analysis.git
```

Be sure to add this repository to your PYTHONPATH to be able to use it properly.

---

# Using

To use the code, simply import a given I/O file of interest with:
```python
import orbit_io
```

---

# License
See the LICENSE.md file for info, but feel free to use these tools for free! (ASCL submission soon, so stay tuned.)
